Rails.application.routes.draw do
  post 'inside', to: 'location#inside?'
  get 'index', to: 'location#index'
  get 'show_given_areas', to: 'given_areas#index'
  post 'create', to: 'location#create'
  post 'show', to: 'location#show'
end
