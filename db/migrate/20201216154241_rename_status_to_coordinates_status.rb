class RenameStatusToCoordinatesStatus < ActiveRecord::Migration[5.2]
  def change
    rename_column :points, :status, :coordinates_status
  end
end
