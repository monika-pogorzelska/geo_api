class AddStatusToPoint < ActiveRecord::Migration[5.2]
  def change
    add_column :points, :status, :string
  end
end
