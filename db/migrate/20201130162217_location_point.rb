class LocationPoint < ActiveRecord::Migration[5.2]
  def change
    create_table :points do |t|
      t.string :name
      t.point :coordinates

      t.timestamps
    end
  end
end
