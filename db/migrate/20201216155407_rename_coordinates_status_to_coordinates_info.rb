class RenameCoordinatesStatusToCoordinatesInfo < ActiveRecord::Migration[5.2]
  def change
    rename_column :points, :coordinates_status, :coordinates_info
  end
end
