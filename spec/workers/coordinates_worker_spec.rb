require 'rails_helper'

describe CoordinatesWorker do
  it 'goes into the jobs array' do
    expect { described_class.perform_async(5) }
      .to change(described_class.jobs, :size).by(1)
  end

  it 'checks coordinates' do
    expect_any_instance_of(StoreLocationCoordinates).to receive(:call).with(5)
    subject.perform(5)
  end
end