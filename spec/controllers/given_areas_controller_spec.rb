require 'rails_helper'

RSpec.describe GivenAreasController, type: :controller do
  describe 'index' do
    let(:locations) { build(:locations) }

    it 'shows locations' do
      allow(LocationsFromGeojson)
        .to receive(:read_file).and_return(locations)

      get :index

      expect(response).to have_http_status(:ok)
      parsed_response = JSON.parse(response.body, symbolize_names: true)
      expect(parsed_response[:features].count).to eq(2)
      expect(parsed_response[:features].first).to eq(
        {
          "type": "Feature",
          "geometry": {
            "type": "Polygon",
            "coordinates": [
              [
                [4,1],
                [4,6], 
                [9,6], 
                [9,1], 
                [4,1]
              ]
            ]
          }
        }
      )
    end
  end
end