require 'rails_helper'

RSpec.describe LocationController, type: :controller do
  describe 'create' do
    context 'sunny path' do
      let(:params) {{ name: 'Amsterdam' }}
     
      it 'creates location' do
        expect { post :create, params: params }
          .to change(CoordinatesWorker.jobs, :size).by(1)

        expect(response).to have_http_status(:ok)
        parsed_response = JSON.parse(response.body, symbolize_names: true)
       
        location = Point.find_by(name: params[:name])
        expect(location).to be_present

        expect(parsed_response[:id]).to eq(location.id)
      end
    end

    context 'create fails' do
      let(:wrong_input) {{}}

      it 'returns error' do
        expect { post :create, params: wrong_input }
          .to change(CoordinatesWorker.jobs, :size).by(0)
        
        expect(response).to have_http_status(422)

        parsed_response = JSON.parse(response.body, symbolize_names: true)
        expect(parsed_response[:errors]).to eq(
          ["Name is invalid"])
      end
    end
  end

  describe 'show' do
    context 'sunny path' do
      let(:params) {{ id: 4 }}

      it 'shows location' do
        create(:point)

        post :show, params: params

        expect(response).to have_http_status(200)
        parsed_response = JSON.parse(response.body, symbolize_names: true)
        expect(parsed_response).to eq(
          name: 'Cape Town',
          coordinates: {
            longitude: 18.417396,
            latitude: -33.928992
          },
          inside?: false,
          coordinates_info: 'ok'
        )
      end
    end

    context 'location doesnt exist' do
      let(:params) {{ id: 44 }}

      it 'returns error message' do
        post :show, params: params
        expect(response).to have_http_status(404)

        parsed_response = JSON.parse(response.body, symbolize_names: true)
      
        expect(parsed_response[:error]).to eq(
          "Couldn't find Point with 'id'=44")
      end
    end
  end
end
