require 'rails_helper'

describe LocationsFromGeojson do
  let(:locations) { described_class.read_file }

  it 'has proper structure' do
    locations[:features].each do |feature|
      expect(feature[:geometry][:coordinates]).to be_kind_of(Array)
    end
  end
end
