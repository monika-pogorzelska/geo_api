require 'rails_helper'

describe QueryBuilder do
  let(:polygon) do
    {
      "type": "FeatureCollection",
      "features": [
        {
          "type": "Feature",
          "properties": {},
          "geometry": {
            "type": "Polygon",
            "coordinates": [
              [
                [4,1],
                [4,6], 
                [9,6], 
                [9,1], 
                [4,1]
              ]
            ]
          }
        }
      ]
    }
  end

  let(:point_out) { OpenStruct.new(x: 2, y: 4) }
  let(:point_in) { OpenStruct.new(x:7, y:3) }
  let(:top_edge) { OpenStruct.new(x:5, y:6) }
  let(:left_edge) { OpenStruct.new(x:4, y:4) }
  let(:bottom_edge) { OpenStruct.new(x:6, y:1) }
  let(:right_edge) { OpenStruct.new(x:9, y:3) }
  let(:point_close_to_border_in) { OpenStruct.new(x:5, y:1.1) }
  let(:point_close_to_border_out) { OpenStruct.new(x:3.99, y:2) }

  let(:vertex_1) { OpenStruct.new(x:4, y:1) }
  let(:vertex_2) { OpenStruct.new(x:4, y:6) }
  let(:vertex_3) { OpenStruct.new(x:9, y:6) }
  let(:vertex_4) { OpenStruct.new(x:9, y:1) }

  before do
    allow(LocationsFromGeojson)
      .to receive(:read_file).and_return(polygon)
  end

  subject { described_class.new }

  context 'point is inside' do
    it 'checks point inside' do
      expect(subject.point_inside?(point_in)).to eq(true)
    end

    it 'checks point close to border' do
      expect(subject.point_inside?(point_close_to_border_in)).to eq(true)
    end

    it 'checks bottom left vertex' do
      expect(subject.point_inside?(vertex_1)).to eq(true)
    end

    it 'checks point on left edge' do
      expect(subject.point_inside?(left_edge)).to eq(true)
    end

    it 'checks point on bottom edge' do
      expect(subject.point_inside?(bottom_edge)).to eq(true)
    end
  end

  
  context 'point is outside' do
    it 'checks if point is outside' do
      expect(subject.point_inside?(point_out)).to eq(false)
    end

    it 'checks if point ...' do
      expect(subject.point_inside?(top_edge)).to eq(false)
    end

    it 'checks if point on right edge is inside' do
      expect(subject.point_inside?(right_edge)).to eq(false)
    end

     it 'checks if point close to border is outside' do
      expect(subject.point_inside?(point_close_to_border_out)).to eq(false)
    end

    it 'checks if point ...' do
      expect(subject.point_inside?(vertex_2)).to eq(false)
    end

    it 'checks if point ...' do
      expect(subject.point_inside?(vertex_3)).to eq(false)
    end

    it 'checks if point ...' do
      expect(subject.point_inside?(vertex_4)).to eq(false)
    end
  end
end