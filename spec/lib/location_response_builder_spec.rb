require 'rails_helper'

describe LocationResponseBuilder do
  let(:location) { build(:point) }

  subject { described_class.new(location) }

  it 'builds response for location show' do
    expect(subject.build).to eq(
      {
        "name": "Cape Town",
        "coordinates": {
          "longitude": 18.417396,
          "latitude": -33.928992
        },
        "inside?": false,
        "coordinates_info": 'ok'
      }
    )
  end
end