require 'rails_helper'

describe StoreLocationCoordinates do
  let(:location) { create(:point_without_coordinates) }

  before do
    allow_any_instance_of(FindLocationCoordinates)
      .to receive(:call)
      .with(location.name)
      .and_return(OpenStruct.new(success?: true, payload: '138.73,35.36'))
  end

  subject { described_class.new }

  it 'updates location with coordinates' do
    subject.call(location.id)
    updated_location = Point.find(location.id)

    expect(updated_location.coordinates.x).to eq(138.73)
    expect(updated_location.coordinates.y).to eq(35.36)
    expect(updated_location.coordinates_info).to eq('ok')
  end
end