require 'rails_helper'

describe FindLocationCoordinates do
  subject(:api) { described_class.new }

  # TODO use webmock
  let(:api_response_success) { instance_double(HTTParty::Response, parsed_response: parsed_response, code: 200)}
  let(:parsed_response) do
    [
      {
        "lon" => "18.4263523",
        "lat" => "-33.9224221"
      }
    ]
  end

  let(:api_response_error) { instance_double(HTTParty::Response, code: 404)}

  context 'http request success' do
    before do
      allow(FindLocationCoordinates).to receive(:get).and_return(api_response_success)
    end
    
    it 'finds location coordinates' do
      response = api.call('Cape Town')
      expect(response.success?).to eq(true)
      expect(response.payload).to eq("18.4263523,-33.9224221")
    end
  end

  context 'http request failure' do
    before do
      allow(FindLocationCoordinates).to receive(:get).and_return(api_response_error)
    end
    
    it 'finds location coordinates' do
      response = api.call('Wladywostok')
      expect(response.success?).to eq(false)
      expect(response.error).to eq("coordinates not found")
    end
  end
end