require 'rails_helper'

describe CreateLocation do
  let(:file) do
    {
      type: "FeatureCollection",
      features: [
        geometry: {
          coordinates: [
            [
              [ 22.85, -13.068 ],
              [ 33.39, -22.26 ],
              [ 36.38, -2.81 ],
              [ 22.85, -13.068 ]
            ]
          ]
        }
      ]
    }
  end

  describe 'call' do
    let(:subject) { described_class.new(file)}

    it 'creates location in database' do
      subject.call
      expect(Location.first.coordinates).to eq(
        "((22.85,-13.068),(33.39,-22.26),(36.38,-2.81),(22.85,-13.068))"
      )
    end
  end
end