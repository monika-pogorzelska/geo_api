FactoryBot.define do
  factory :point do
    name { 'Cape Town' }
    coordinates { '18.417396,-33.928992' }
    coordinates_info { 'ok' }
    id { 4 }
  end

  factory :point_without_coordinates, class: 'Point' do
    name { 'Fuji' }
    id { 5 }
  end
  
  factory :locations, class:Hash do
    type { "FeatureCollection" }
    features {
      [
        {
          type: "Feature",
          geometry: {
            type: "Polygon",
            coordinates: [[[4,1], [4,6], [9,6], [9,1], [4,1]]]
          }
        },
        {
          type: "Feature",
          geometry: {
            type: "Polygon",
            coordinates: [[[3,4], [3,8], [9,8], [9,4], [6,2], [3,4]]]
          }
        }
      ]
    }
      
    initialize_with { attributes } 
  end
end
