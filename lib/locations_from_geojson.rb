class LocationsFromGeojson
  PATH = 'lib/assets/given_areas.json'

  def self.read_file
    JSON.parse(File.read(PATH), symbolize_names: true)
  end
end
