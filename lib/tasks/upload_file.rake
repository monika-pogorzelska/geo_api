require_relative '../locations_from_geojson.rb'

namespace :db do
  desc 'create locations from GeoJSON'
  task create_locations: :environment do
    puts "Task is running for environment: #{Rails.env}"
    puts 'create locations from file'

    file = LocationsFromGeojson.read_file

    CreateLocation.new(file).call

    puts 'Locations have beed created'
  end
end
