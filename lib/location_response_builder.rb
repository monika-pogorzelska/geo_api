class LocationResponseBuilder
  attr_accessor :location

  def initialize(location)
    @location = location
  end


  def build
    coordinates = location.coordinates
    {
      name: location.name,
      coordinates: lonlat(coordinates),
      inside?: inside?(coordinates),
      coordinates_info: location.coordinates_info
    }
  end

  private

  def lonlat(coordinates)
    if coordinates
      {
        longitude: coordinates.x,
        latitude: coordinates.y
      }
    end
  end

  def inside?(coordinates)
    if coordinates
      QueryBuilder.new.point_inside?(coordinates)
    else
      false
    end
  end
end