class QueryBuilder
  def point_inside?(point)
    polygons.any? { |polygon| point_inside_polygon?(point, polygon) }
  end

  private

  def point_inside_polygon?(point, polygon)
    return true if winding_number(point, polygon) != 0
  end

  def polygons
    @polygons ||= begin
      file = LocationsFromGeojson.read_file

      file[:features].map do |feature|
        verticies = feature[:geometry][:coordinates][0]
        verticies.map do |vertex|
          OpenStruct.new(x: vertex[0], y: vertex[1])
        end
      end
    end
  end

  def is_left(vertex, next_vertex, point)
    ((next_vertex.x - vertex.x) * (point.y - vertex.y) - 
    (point.x -  vertex.x) * (next_vertex.y - vertex.y))
  end

  def winding_number(point, polygon)
    wn = 0
    
    polygon[0..-2].each_with_index do |vertex, index|
      if vertex.y <= point.y
        wn += 1 if upward_crossing(point, vertex, polygon[index+1])
      else
        wn -= 1 if downward_crossing(point, vertex, polygon[index+1])
      end
    end
    wn
  end

  def upward_crossing(point, vertex, next_vertex)
    if (next_vertex.y > point.y)
      is_left(vertex, next_vertex, point) > 0
    end
  end

  def downward_crossing(point, vertex, next_vertex)
    if next_vertex.y <= point.y
      is_left(vertex, next_vertex, point) < 0
    end
  end
end