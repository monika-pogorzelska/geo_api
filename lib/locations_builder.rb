class LocationsBuilder
  def self.build(locations)
    {
      "type": "FeatureCollection",
      "features": build_features(locations)
    }
  end

  private

  def self.build_features(locations)
    locations.map do |location|
      build_polygon(location.coordinates)
    end
  end

  def self.build_polygon(coordinates)
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          transform_coordinates(coordinates)
        ]
      }
    }
  end

  def self.transform_coordinates(coordinates)
    coordinates.split(/\),\(/).map {|i| i.gsub(/[()]/, '').split(',')}
  end
end