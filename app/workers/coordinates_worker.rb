class CoordinatesWorker
  include Sidekiq::Worker

  def perform(location_id)
    StoreLocationCoordinates.new.call(location_id)
  end
end