class ApplicationController < ActionController::API
  rescue_from StandardError, with: :render_standard_error
  rescue_from ActiveRecord::RecordNotFound, with: :render_record_not_found

  private
  
  def render_standard_error(exception)
    Rails.logger.error("#{exception.message}")
    render json: { error: exception.message }, status: :internal_server_error
  end

  def render_record_not_found(exception)
    render json: { error: exception.message }, status: :not_found
  end
end