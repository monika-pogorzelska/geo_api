class GivenAreasController < ApplicationController
  def index
    given_areas = LocationsFromGeojson.read_file

    render json: given_areas
  end
end