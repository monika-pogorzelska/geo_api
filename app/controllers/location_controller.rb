class LocationController < ApplicationController
  def create
    location = Point.find_or_create_by(name: params[:name])
    
    if location.valid?
      CoordinatesWorker.perform_async(location.id)
      render json: { id: location.id }
    else
      render json: { errors: location.errors.full_messages}, 
                     status: :unprocessable_entity
    end
  end

  def show
    location = Point.find(params[:id])

    render json: LocationResponseBuilder.new(location).build
  end
end
