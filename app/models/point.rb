class Point < ApplicationRecord
  validates :name, format: { with: /\A[a-zA-Z\s]+\z/ }
end