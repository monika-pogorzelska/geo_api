class Location < ApplicationRecord
  scope :contains, -> (point) { where("point ? <@ coordinates", point) }
end
