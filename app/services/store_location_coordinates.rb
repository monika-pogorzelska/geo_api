class StoreLocationCoordinates
  def call(location_id)
    location = Point.find(location_id)
    coordinates = FindLocationCoordinates.new.call(location.name)
    message = coordinates.success? ? 'ok' : coordinates.error
    location.update!(
      coordinates: coordinates.payload, 
      coordinates_info: message
    )
  end
end