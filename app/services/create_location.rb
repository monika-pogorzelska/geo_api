class CreateLocation
  def initialize(file)
    @file = file
  end

  def call
    @file[:features].each.with_index(1) do |feature, index|
      polygon = map_coordinates(feature[:geometry][:coordinates].first)
      create_location(index, polygon)
    end
  end

  private

  def map_coordinates(coordinates)
    coordinates.map {|point| "(#{point[0]},#{point[1]})"}.join(',')
  end

  def create_location(index, poligon)
    Location.create!(
      name: "Location_#{index}",
      coordinates: "(#{poligon})"
    )
  end
end
