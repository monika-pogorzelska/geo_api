class FindLocationCoordinates
  include HTTParty
  base_uri 'https://eu1.locationiq.com/v1/search.php?'
  default_params key: Rails.application.credentials.api_key

  def call(location_name)
    query_data = { q: location_name, format: 'json' }.to_query
    
    response = self.class.get(query_data)

    if response.code == 200
      coordinates = format_coordinates(response.parsed_response)
      OpenStruct.new(success?: true, payload: coordinates)
    else
      OpenStruct.new(success?: false, error: error_message(response.code))
    end
  end

  private

  def format_coordinates(data)
    "#{data.first['lon']},#{data.first['lat']}"
  end

  def error_message(code)
    code == 404 ? 'coordinates not found' : 'something went wrong'
  end
end